from utils.yield_data import mines_data, compute_yield_data
from datetime import datetime, timedelta
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont 

CRABADA_ADDRESSES = ['0xfda125add7b306cb8a5f6df3fb4b7696d1c262e2', 
                     '0xe4ba7dd8f2f9563aa9817df69c0a8f79c4d51327',
                     '0x44a40ca311058fef527b62ec359a0a4eb5cf296e']

mines_data_arr = [mines_data(address) for address in CRABADA_ADDRESSES]

mines_yield_dict = {
    'tus': {}, 
    'cra': {}
}

computed_yield = compute_yield_data(mines_data_arr, mines_yield_dict)

today = datetime.now()
dates = [(today + timedelta(days=i)).strftime("%d/%m/%Y") for i in range(0 - today.weekday(), 7 - today.weekday())]

WTD_TUS_YIELD = 0
WTD_CRA_YIELD = 0

for date in dates: 
    if (date in mines_yield_dict['cra'].keys()):
        WTD_CRA_YIELD += mines_yield_dict['cra'][date]
    if (date in mines_yield_dict['tus'].keys()):
        WTD_TUS_YIELD += mines_yield_dict['tus'][date]

img = Image.open('crabadao-img.jpg')

I1 = ImageDraw.Draw(img)

myFont = ImageFont.truetype('roboto.bold.ttf', 38)

I1.text((416, 550), str(WTD_TUS_YIELD), font=myFont, fill =(255,255, 255))
img.show()

