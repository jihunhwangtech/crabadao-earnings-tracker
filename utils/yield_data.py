import requests
from datetime import datetime, timedelta
import pytz

def mines_data(user_address):
    MINES_URL = 'https://idle-api.crabada.com/public/idle/mines/'
    payload = {
        'user_address': user_address, 
        'page': 1,
        'status': 'close',
        'limit': 100
    }
    return requests.get(MINES_URL, params = payload).json()

def compute_yield_data(mines_data_arr, mines_yield_dict):
    for mines_yield_output in mines_data_arr:
        for row in mines_yield_output['result']['data']:
            mine_date = datetime.fromtimestamp(row['start_time']).strftime("%d/%m/%Y")

            if (mine_date not in mines_yield_dict['tus'].keys()):
                mines_yield_dict['tus'][mine_date] = 0
                mines_yield_dict['cra'][mine_date] = 0

            daily_tus_reward = row['miner_tus_reward'] / (10 ** 18)
            daily_cra_reward = row['miner_cra_reward'] / (10 ** 18)

            mines_yield_dict['tus'][mine_date] += daily_tus_reward
            mines_yield_dict['cra'][mine_date] += daily_cra_reward
    return mines_yield_dict
    
def get_total_crabadao_nfts():
    SNOWTRACE_URL = 'https://api.snowtrace.io/api'
    crabadao_nft_addr = '0x529D5c4cD24879fD31e346CF6EEa56392641C156'
    api_token_key = 'XXX'

    payload = {
        'module': 'stats',
        'action': 'tokensupply',
        'contractaddress': crabadao_nft_addr,
        'apikey': api_token_key
    }
    
    result = requests.get(SNOWTRACE_URL, params=payload).json()
    return int(result['result'])


 